class Test {
  final String test = 'hello world';

  String go() {
    return test;
  }
}

void main() {
  Symbol x = Symbol('go');
  Invocation call = new Invocation.method(x, [null]);
  Test test = new Test();
  final a = test.noSuchMethod(call);

  print(a);
}