import 'dart:io';
import 'package:protobuf/protobuf.dart';
import 'package:myriad/myriad.dart';
import 'package:myriad/src/proto/myriad.pb.dart';

const PORT = 9876;

void main() {
  MyriadServer srv = new MyriadServer(port: PORT);

  srv.listen((GeneratedMessage message, Datagram data) {
    print('Client sent from ${data.address.address}:${data.port}');
    if (message is AuthenticationFrame) {
      print('Authentication frame received: $message');
    }
    if (message is PeersFrame) {
      print('Peers frame received: $message');
    }
  });
}
