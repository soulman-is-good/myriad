import 'dart:io';
import 'dart:async';
import 'dart:isolate';
import 'package:protobuf/protobuf.dart';
import 'package:uuid/uuid.dart';
import 'package:myriad/myriad.dart';
import 'package:myriad/src/proto/myriad.pb.dart';

const PORT = 9876;
Uuid uuid = new Uuid();
List<String> strings = [
  'Instance',
  'aklsjdalksjdlaksjd',
  'woiiowiowejf',
  'jdsjf',
  'WARRIOR',
];
int limit = 5;

void connectClient(int index) {
  MyriadServer srv = new MyriadServer(port: 9890 + index);
  String id = uuid.v4();
  String message = '${strings[index]} $index:$id';

  print('Starting $message');

  srv.listen((GeneratedMessage message, Datagram data) {
    if (message is PeersFrame) {
      print(message.peers.map((Peer peer) => peer.id));
      srv.close();
    }
  });

  new Timer(new Duration(seconds: index), () {
    AuthenticationFrame req = new AuthenticationFrame()
      ..id = id;

    srv.send(req, PORT, InternetAddress.loopbackIPv4);
  });
}

void main() {
  MyriadServer srv = new MyriadServer(port: PORT);
  int connections = 0;

  srv.listen((GeneratedMessage message, Datagram data) {
    print('Client sent from ${data.address.address}:${data.port}');
    if (message is AuthenticationFrame) {
      print('Authentication frame received: $message');
    }
    if (++connections == limit) {
      print('END');
      // srv.broadcast([0x01]);
      srv.close();
    }
  });

  for (int i = 0; i < limit; i++) {
    Isolate.spawn(connectClient, i);
  }
}
