import 'dart:io';
import 'dart:async';
import 'package:protobuf/protobuf.dart';
import 'package:uuid/uuid.dart';
import 'package:myriad/src/proto/myriad.pb.dart';

// 233.252.18.0-233.255.255.255	is unassigned
final InternetAddress multicastAddress = new InternetAddress('233.255.255.255');
final uuid = new Uuid();
typedef GeneratedMessage MessageCreator(List<int> bytes);

class _Peer {
  final InternetAddress ip;
  final int port;

  _Peer(this.ip, this.port);
  _Peer.fromDatagram(Datagram packet): this(packet.address, packet.port);

  @override
  int get hashCode => (ip.hashCode << 2 | port);

  bool operator ==(dynamic other) => hashCode == other.hashCode;
}

List<MessageCreator> _lookupMessage = [
  null,
  (List<int> bytes) => new AuthenticationFrame.fromBuffer(bytes),
  (List<int> bytes) => new PeersFrame.fromBuffer(bytes),
];

class MyriadServer {
  RawDatagramSocket _socket;
  final int _port;
  final InternetAddress _host;
  final Map<List<int>, _Peer> _clients;
  final String _uuid;
  bool _resolved = false;

 MyriadServer({InternetAddress host, int port}):
  _uuid = uuid.v4(),
  _port = port ?? 9876,
  _host = host ?? InternetAddress.anyIPv4,
  _clients = new Map();

  Future<RawDatagramSocket> _bind() async {
    if (!_resolved) {
      List<NetworkInterface> addresses = await NetworkInterface.list();

      _clients[uuid.parse(_uuid)] = new _Peer(addresses.first.addresses.first, _port);
      _resolved = true;
    }
    if (_socket == null) {
      _socket = await RawDatagramSocket.bind(_host, _port);
      _socket.multicastHops = 10;
      _socket.multicastLoopback = false;
      _socket.joinMulticast(multicastAddress);
    }

    return _socket;
  }

  void close() {
    if (_socket != null) {
      _socket.close();
      _socket = null;
    }
  }

  void listen(void onData(GeneratedMessage message, Datagram packet)) {
    // TODO: Breaking change: unsubscribe!
    // TODO: Think of extending from Stream
    _bind().then((RawDatagramSocket socket) {
      socket.listen((RawSocketEvent event) {
        Datagram packet = socket.receive();

        if (packet != null) {
          List<int> buffer = packet.data;
          MessageCreator creator = _lookupMessage[buffer[0]];

          if (creator != null) {
            GeneratedMessage message = creator(buffer.sublist(1).toList());

            if (message is AuthenticationFrame) {
              List<int> id = uuid.parse(message.id);
              PeersFrame frame = new PeersFrame();

              _clients.forEach((List id, _Peer peer) {
                Peer packPeer = new Peer();

                packPeer.id = uuid.unparse(id);
                packPeer.ip = peer.ip.address;
                packPeer.port = peer.port;
                frame.peers.add(packPeer);
              });
              send(frame, packet.port, packet.address);
              _clients[id] = new _Peer(packet.address, packet.port);
            } else if (message is PeersFrame) {
              message.peers.forEach((Peer peer) {
                _clients[uuid.parse(peer.id)] = new _Peer(
                  new InternetAddress(peer.ip),
                  peer.port,
                );
              });
            }

            onData(message, packet);
          } else {
            print('Unknown buffer');
            print(buffer);
          }
        }
      });
    });
  }

  int _getMessageType(GeneratedMessage message) {
    if (message is AuthenticationFrame) {
      return 1;
    } else if (message is PeersFrame) {
      return 2;
    }

    return 0;
  }
  
  Future send(GeneratedMessage message, int port, [InternetAddress address]) async {
    RawDatagramSocket socket = await _bind();
    List<int> bytes = [_getMessageType(message)]..addAll(message.writeToBuffer());

    socket.send(bytes, address ?? multicastAddress, port);
  }

  Future broadcast(List<int> bytes) async {
    RawDatagramSocket socket = await _bind();

    _clients.forEach((List<int> ids, _Peer peer) {
      socket.send(bytes, peer.ip, peer.port);
    });
  }
}
