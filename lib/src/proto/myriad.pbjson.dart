///
//  Generated code. Do not modify.
//  source: myriad.proto
///
// ignore_for_file: non_constant_identifier_names,library_prefixes,unused_import

const AuthenticationFrame$json = const {
  '1': 'AuthenticationFrame',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 9, '10': 'id'},
  ],
};

const PeersFrame$json = const {
  '1': 'PeersFrame',
  '2': const [
    const {'1': 'peers', '3': 1, '4': 3, '5': 11, '6': '.myriad.Peer', '10': 'peers'},
  ],
};

const Peer$json = const {
  '1': 'Peer',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 9, '10': 'id'},
    const {'1': 'ip', '3': 2, '4': 1, '5': 9, '10': 'ip'},
    const {'1': 'port', '3': 3, '4': 1, '5': 5, '10': 'port'},
  ],
};

