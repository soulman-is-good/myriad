///
//  Generated code. Do not modify.
//  source: myriad.proto
///
// ignore_for_file: non_constant_identifier_names,library_prefixes,unused_import

// ignore: UNUSED_SHOWN_NAME
import 'dart:core' show int, bool, double, String, List, override;

import 'package:protobuf/protobuf.dart' as $pb;

class AuthenticationFrame extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = new $pb.BuilderInfo('AuthenticationFrame', package: const $pb.PackageName('myriad'))
    ..aOS(1, 'id')
    ..hasRequiredFields = false
  ;

  AuthenticationFrame() : super();
  AuthenticationFrame.fromBuffer(List<int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  AuthenticationFrame.fromJson(String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  AuthenticationFrame clone() => new AuthenticationFrame()..mergeFromMessage(this);
  AuthenticationFrame copyWith(void Function(AuthenticationFrame) updates) => super.copyWith((message) => updates(message as AuthenticationFrame));
  $pb.BuilderInfo get info_ => _i;
  static AuthenticationFrame create() => new AuthenticationFrame();
  static $pb.PbList<AuthenticationFrame> createRepeated() => new $pb.PbList<AuthenticationFrame>();
  static AuthenticationFrame getDefault() => _defaultInstance ??= create()..freeze();
  static AuthenticationFrame _defaultInstance;
  static void $checkItem(AuthenticationFrame v) {
    if (v is! AuthenticationFrame) $pb.checkItemFailed(v, _i.qualifiedMessageName);
  }

  String get id => $_getS(0, '');
  set id(String v) { $_setString(0, v); }
  bool hasId() => $_has(0);
  void clearId() => clearField(1);
}

class PeersFrame extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = new $pb.BuilderInfo('PeersFrame', package: const $pb.PackageName('myriad'))
    ..pp<Peer>(1, 'peers', $pb.PbFieldType.PM, Peer.$checkItem, Peer.create)
    ..hasRequiredFields = false
  ;

  PeersFrame() : super();
  PeersFrame.fromBuffer(List<int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  PeersFrame.fromJson(String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  PeersFrame clone() => new PeersFrame()..mergeFromMessage(this);
  PeersFrame copyWith(void Function(PeersFrame) updates) => super.copyWith((message) => updates(message as PeersFrame));
  $pb.BuilderInfo get info_ => _i;
  static PeersFrame create() => new PeersFrame();
  static $pb.PbList<PeersFrame> createRepeated() => new $pb.PbList<PeersFrame>();
  static PeersFrame getDefault() => _defaultInstance ??= create()..freeze();
  static PeersFrame _defaultInstance;
  static void $checkItem(PeersFrame v) {
    if (v is! PeersFrame) $pb.checkItemFailed(v, _i.qualifiedMessageName);
  }

  List<Peer> get peers => $_getList(0);
}

class Peer extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = new $pb.BuilderInfo('Peer', package: const $pb.PackageName('myriad'))
    ..aOS(1, 'id')
    ..aOS(2, 'ip')
    ..a<int>(3, 'port', $pb.PbFieldType.O3)
    ..hasRequiredFields = false
  ;

  Peer() : super();
  Peer.fromBuffer(List<int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  Peer.fromJson(String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  Peer clone() => new Peer()..mergeFromMessage(this);
  Peer copyWith(void Function(Peer) updates) => super.copyWith((message) => updates(message as Peer));
  $pb.BuilderInfo get info_ => _i;
  static Peer create() => new Peer();
  static $pb.PbList<Peer> createRepeated() => new $pb.PbList<Peer>();
  static Peer getDefault() => _defaultInstance ??= create()..freeze();
  static Peer _defaultInstance;
  static void $checkItem(Peer v) {
    if (v is! Peer) $pb.checkItemFailed(v, _i.qualifiedMessageName);
  }

  String get id => $_getS(0, '');
  set id(String v) { $_setString(0, v); }
  bool hasId() => $_has(0);
  void clearId() => clearField(1);

  String get ip => $_getS(1, '');
  set ip(String v) { $_setString(1, v); }
  bool hasIp() => $_has(1);
  void clearIp() => clearField(2);

  int get port => $_get(2, 0);
  set port(int v) { $_setSignedInt32(2, v); }
  bool hasPort() => $_has(2);
  void clearPort() => clearField(3);
}

